
<?php

session_start();

require_once "db.php";

$etat = "base";

// de base
$requete = "SELECT * FROM ticket";
$resultat = mysqli_query($lien_bdd,$requete);

while ($rowticket = mysqli_fetch_assoc($resultat)) {
    $_listeticket[] = $rowticket;
}
// nouveau
$requetenouveau = "SELECT * From ticket WHERE ticket.etat = 'Nouveau'";
$resultatnouveau = mysqli_query($lien_bdd,$requetenouveau);
    
while ($rowticketnouveau = mysqli_fetch_assoc($resultatnouveau)) {
$_listeticketnouveau[] = $rowticketnouveau;
}

$requetenouveaureste = "SELECT * FROM ticket WHERE ticket.etat != 'Nouveau'";
$resultatnouveaureste = mysqli_query($lien_bdd,$requetenouveaureste);
    
while ($rowticketnouveaureste = mysqli_fetch_assoc($resultatnouveaureste)) {
$_listeticketnouveaureste[] = $rowticketnouveaureste;
}
// encours
$requeteencours = "SELECT * From ticket WHERE ticket.etat = 'En cours'";
$resultatencours = mysqli_query($lien_bdd,$requeteencours);
    
while ($rowticketencours = mysqli_fetch_assoc($resultatencours)) {
$_listeticketencours[] = $rowticketencours;
}

$requeteencoursreste = "SELECT * FROM ticket WHERE ticket.etat != 'En cours'";
$resultatencoursreste = mysqli_query($lien_bdd,$requeteencoursreste);
    
while ($rowticketencoursreste = mysqli_fetch_assoc($resultatencoursreste)) {
$_listeticketencoursreste[] = $rowticketencoursreste;
}
//terminé
$requetetermine = "SELECT * From ticket WHERE ticket.etat = 'Terminé'";
$resultattermine = mysqli_query($lien_bdd,$requetetermine);
    
while ($rowtickettermine = mysqli_fetch_assoc($resultattermine)) {
$_listetickettermine[] = $rowtickettermine;
}

$requeteterminereste = "SELECT * FROM ticket WHERE ticket.etat != 'Terminé'";
$resultatterminereste = mysqli_query($lien_bdd,$requeteterminereste);
    
while ($rowticketterminereste = mysqli_fetch_assoc($resultatterminereste)) {
$_listeticketterminereste[] = $rowticketterminereste;
}

if (isset($_POST['nouveau'])) {
    $etat = "nouveau";
}
if (isset($_POST['encours'])) {
    $etat = "encours" ;  
}
if (isset($_POST['termine'])) {
    $etat = "termine";
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Admin</title>
<link rel="stylesheet" type="text/css" href="style.css" media="all" />
</head>
<body>
<?php
//nouveau
if ($etat == "nouveau") {
    if(count($_listeticketnouveau)){
        echo "<ul class=\"debut1\">";
        for($i=0; $i<count($_listeticketnouveau); $i++){
            echo "<li><a href='ticketadmin.php?idticket=".$_listeticketnouveau[$i]['ID8chiffres']."'>".$_listeticketnouveau[$i]['Titre']."</a></li>";
        }
        echo "</ul>";
if(count($_listeticketnouveau)){
        echo "<ul class=\"suite1\">";
        for($i=0; $i<count($_listeticketnouveau); $i++){
            echo "<a><br>".$_listeticketnouveau[$i]['etat']."</a>";
            
        }
        echo "</ul>";
        }
}
if(count($_listeticketnouveaureste)){
    echo "<ul class=\"debut2\">";
    for($i=0; $i<count($_listeticketnouveaureste); $i++){
        echo "<li><a href='ticketadmin.php?idticket=".$_listeticketnouveaureste[$i]['ID8chiffres']."'>".$_listeticketnouveaureste[$i]['Titre']."</a></li>";
    }
    echo "</ul>";
if(count($_listeticketnouveaureste)){
    echo "<ul class=\"suite2\">";
    for($i=0; $i<count($_listeticketnouveaureste); $i++){
        echo "<a><br>".$_listeticketnouveaureste[$i]['etat']."</a>";
        
    }
    echo "</ul>";
    }
}
} 
//encours
if ($etat == "encours") {
    if(count($_listeticketencours)){
        echo "<ul class=\"debut1\">";
        for($i=0; $i<count($_listeticketencours); $i++){
            echo "<li><a href='ticketadmin.php?idticket=".$_listeticketencours[$i]['ID8chiffres']."'>".$_listeticketencours[$i]['Titre']."</a></li>";
        }
        echo "</ul>";
if(count($_listeticketencours)){
        echo "<ul class=\"suite1\">";
        for($i=0; $i<count($_listeticketencours); $i++){
            echo "<a><br>".$_listeticketencours[$i]['etat']."</a>";
            
        }
        echo "</ul>";
        }
}
if(count($_listeticketencoursreste)){
    echo "<ul class=\"debut2\">";
    for($i=0; $i<count($_listeticketencoursreste); $i++){
        echo "<li><a href='ticketadmin.php?idticket=".$_listeticketencoursreste[$i]['ID8chiffres']."'>".$_listeticketencoursreste[$i]['Titre']."</a></li>";
    }
    echo "</ul>";
if(count($_listeticketencoursreste)){
    echo "<ul class=\"suite2\">";
    for($i=0; $i<count($_listeticketencoursreste); $i++){
        echo "<a><br>".$_listeticketencoursreste[$i]['etat']."</a>";
        
    }
    echo "</ul>";
    }
}
} 
//terminé
if ($etat == "termine") {
    if(count($_listetickettermine)){
        echo "<ul class=\"debut1\">";
        for($i=0; $i<count($_listetickettermine); $i++){
            echo "<li><a href='ticketadmin.php?idticket=".$_listetickettermine[$i]['ID8chiffres']."'>".$_listetickettermine[$i]['Titre']."</a></li>";
        }
        echo "</ul>";
if(count($_listetickettermine)){
        echo "<ul class=\"suite2\">";
        for($i=0; $i<count($_listetickettermine); $i++){
            echo "<a><br>".$_listetickettermine[$i]['etat']."</a>";
            
        }
        echo "</ul>";
        }
}
if(count($_listeticketterminereste)){
    echo "<ul class=\"debut2\">";
    for($i=0; $i<count($_listeticketterminereste); $i++){
        echo "<li><a href='ticketadmin.php?idticket=".$_listeticketterminereste[$i]['ID8chiffres']."'>".$_listeticketterminereste[$i]['Titre']."</a></li>";
    }
    echo "</ul>";
if(count($_listeticketterminereste)){
    echo "<ul class=\"suite2\">";
    for($i=0; $i<count($_listeticketterminereste); $i++){
        echo "<a><br>".$_listeticketterminereste[$i]['etat']."</a>";
        
    }
    echo "</ul>";
    }
}
}
//base
if ($etat == "base"){
if(count($_listeticket)){
    echo "<ul class=\"debut0\">";
    for($i=0; $i<count($_listeticket); $i++){
        echo "<li><a href='ticketadmin.php?idticket=".$_listeticket[$i]['ID8chiffres']."'>".$_listeticket[$i]['Titre']."</a></li>";
    }
    echo "</ul>";

if(count($_listeticket)){
    echo "<ul class=\"suite0\">";
    for($i=0; $i<count($_listeticket); $i++){
        echo "<a><br>".$_listeticket[$i]['etat']."</a>";
    
    }
    echo "</ul>";
}
}
}


?>
<form method ="POST">
<input class= "tri" type = "submit" name ="nouveau" value = "Nouveau"/>
<input class= "tri" type = "submit" name ="encours" value = "En cours"/>
<input class= "tri" type = "submit" name ="termine" value = "Terminé"/>
</form>
</body>
</html> 