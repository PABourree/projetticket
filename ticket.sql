-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 26 oct. 2020 à 16:56
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ticket`
--

-- --------------------------------------------------------

--
-- Structure de la table `ticket`
--

CREATE TABLE `ticket` (
  `TicketID` int(10) UNSIGNED NOT NULL,
  `Titre` varchar(255) NOT NULL,
  `Contenu` text NOT NULL,
  `Categorie` int(10) UNSIGNED NOT NULL,
  `Mail` varchar(255) NOT NULL,
  `ID8chiffres` varchar(8) NOT NULL,
  `Valide` tinyint(1) NOT NULL DEFAULT '0',
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `etat` varchar(50) NOT NULL DEFAULT 'Nouveau'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `ticket`
--

INSERT INTO `ticket` (`TicketID`, `Titre`, `Contenu`, `Categorie`, `Mail`, `ID8chiffres`, `Valide`, `Date`, `etat`) VALUES
(1, 'pb 1', 'test contenu', 1, 'aze@ty.fr', '1', 0, '2020-10-22 15:22:22', 'Nouveau'),
(2, 'pb 1', 'test contenu', 1, 'aze@ty.fr', '2', 0, '2020-10-22 15:23:43', 'Nouveau'),
(3, 'pb 1', 'test contenu', 1, 'qwer@ty.fr', '3', 0, '2020-10-22 15:23:43', 'Nouveau'),
(4, 'pb 1', 'test contenu', 1, 'aze@ty.fr', '4', 0, '2020-10-22 15:23:43', 'Nouveau'),
(5, 'pb 1', 'test contenu', 1, 'aze@ty.fr', '5', 0, '2020-10-22 15:23:43', 'Nouveau'),
(6, 'pb php1', 'test probleme', 1, 'aze@ty.fr', '46856689', 0, '2020-10-22 16:42:24', 'Nouveau'),
(7, 'test php 3', 'contenu ', 1, 'aze@ty.fr', '1', 0, '2020-10-22 16:43:40', 'Nouveau'),
(8, 'test 34', 'contenu test 4', 1, 'aze@ty.fr', '00000001', 0, '2020-10-22 16:44:49', 'Nouveau'),
(9, 'pb php2', 'test encore', 1, 'aze@ty.fr', '95590209', 0, '2020-10-22 16:46:14', 'Nouveau'),
(10, 'pb php1', 'test', 1, 'aze@ty.fr', '09927368', 0, '2020-10-22 17:04:45', 'Nouveau'),
(11, 'test etat', 'etat', 2, 'a.a@a', '74536132', 0, '2020-10-25 14:02:48', 'Nouveau'),
(12, 'test admin2', 'test admin2', 1, 'a.a@a', '75006103', 0, '2020-10-26 00:40:22', 'En cours'),
(13, 'test de fin', 'c cassÃ©', 2, 'a.a@a', '76113891', 0, '2020-10-26 10:32:50', 'Nouveau');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`TicketID`),
  ADD KEY `Categorie` (`Categorie`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `TicketID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `ticket_ibfk_1` FOREIGN KEY (`Categorie`) REFERENCES `categorie` (`CategorieID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
